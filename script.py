import pywhatkit

# Mengirim sebuah pesan whatsapp ke nomor yang tertera pada pukul 1:30 PM
pywhatkit.sendwhatmsg("+910123456789", "Hi", 13, 30)

# Sama seperti yang di atas tetapi menutup tab dalam 2 detik setelah mengirim pesan
pywhatkit.sendwhatmsg("+910123456789", "Hi", 13, 30, 15, True, 2)

# Mengirim sebuah gambar ke grup dengan caption "Hello"
# Catatan: Parameter grup di bawah ini merupakan Grup ID (bukan nama grupnya).
# Grup ID biasa ditemukan pada tautan undangan grupnya seperti https://chat.whatsapp.com/DQUblPgyXHO4k5nzsUXiUn
pywhatkit.sendwhats_image("DQUblPgyXHO4k5nzsUXiUn", "Images/Hello.png", "Hello")

# Mengirim gambar ke nomor tertera tanpa caption
pywhatkit.sendwhats_image("+910123456789", "Images/Hello.png")

# Mengirim pesan whatsapp ke grup pada pukul 12:00 AM
pywhatkit.sendwhatmsg_to_group("DQUblPgyXHO4k5nzsUXiUn", "Hey All!", 0, 0)

# Mengirim pesan whatsapp ke grup secara instan
pywhatkit.sendwhatmsg_to_group_instantly("DQUblPgyXHO4k5nzsUXiUn", "Hey All!")

# Memutar sebuah video di YouTube
pywhatkit.playonyt("PyWhatKit")

# Mengubah sebuah gambar menjadi ASCII art
# Pastikan gambar tersimpan di direktori yang sama dengan file python
pywhatkit.image_to_ascii_art("python.png")