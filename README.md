# PyWhatKit: Pustaka Python untuk Otomatisasi WhatsApp dan YouTube

PyWhatKit adalah pustaka Python yang menyediakan berbagai fitur bermanfaat dengan kemudahan penggunaan. Anda tidak perlu melakukan penyiapan tambahan karena pustaka ini dirancang untuk digunakan dengan mudah. Saat ini, PyWhatKit telah menjadi salah satu pustaka paling populer untuk otomatisasi di platform WhatsApp dan YouTube. Tim pengembang secara rutin merilis pembaruan baru yang membawa fitur baru dan perbaikan bug untuk pengguna.

Pustaka ini memiliki fokus pada mengotomatisasi tugas-tugas yang terkait dengan WhatsApp dan YouTube, sehingga membantu pengguna untuk menghemat waktu dan usaha dalam melakukan berbagai aktivitas. Meskipun pustaka ini populer untuk mengotomatisasi WhatsApp dan YouTube, fitur-fiturnya dapat digunakan untuk berbagai keperluan, tergantung pada kreativitas dan kebutuhan pengguna.

Jika Anda mencari cara untuk mengotomatisasi tugas-tugas di WhatsApp atau YouTube, PyWhatKit dapat menjadi pilihan yang sempurna dengan antarmuka yang mudah digunakan dan banyak fitur yang berguna. Jangan ragu untuk mencoba dan eksplorasi berbagai fitur yang ditawarkan oleh pustaka ini.

## Instalasi dan Versi yang didukung
```cmd
pip install pywhatkit
```
PyWhatKit juga tersedia di PyPi:
```cmd
python3 -m pip install pywhatkit
```
```cmd
pip3 install pywhatkit
```
PyWhatKit secara resmi membutuhkan minimal versi Python 3.8+.

## Fitur
- Mengirim pesan ke WhatsApp Grup atau Kontak
- Mengirim gambar ke WhatsApp Grup atau Kontak
- Konversi sebuah gambar menjadi ASCII Art
- Konversi string menjadi tulisan tangan
- Memutar Video YouTube
- Mengirim Email

## Catatan
- Jangan menamai file yang anda buat dengan nama librarynya, seperti "pywhatkit.py". Hal ini karena Python akan mencari modul di direktori saat ini sebelum mencarinya di direktori standar, dan jika ada file dengan nama yang sama, itu dapat menyebabkan konflik dan menyebabkan error saat mengimpor pustaka.
- Kode pada file script.py belum mencakup keseluruhan function yang ada pada library pywhatkit, untuk lebih jelasnya bisa dicek di link github berikut: [Pywhatkit](https://github.com/Ankit404butfound/PyWhatKit/wiki).
